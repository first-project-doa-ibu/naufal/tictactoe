import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  bool oTurn = true;
  List<String> displayXO = ['','','','','','','','','',];

  var myTextStyle = TextStyle(color: Colors.white, fontSize: 30);
  int xScore = 0;
  int oScore = 0;
  int filledboxes = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 72, 71, 71),
      body: Column(
        children: <Widget>[
          Expanded(
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                   Padding(
                     padding: const EdgeInsets.all(20.0),
                     child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                       children: [
                         Text('Player O', style: myTextStyle,),
                         Text(oScore.toString(), style: myTextStyle,),
                       ],
                     ),
                   ),
                   Padding(
                     padding: const EdgeInsets.all(20.0),
                     child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                       children: [
                         Text('Player X', style: myTextStyle,),
                         Text(xScore.toString(), style: myTextStyle,),
                       ],
                     ),
                   ),
                  ],
                ),
          )),
          Expanded(
            flex: 3,
            child: GridView.builder(
              itemCount: 9,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
              itemBuilder: (BuildContext context, index) {
                return GestureDetector(
                  onTap: (){
                    _tapped(index);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: const Color.fromARGB(255, 144, 141, 141))
                    ),
                    child: Center(
                      child: Text(displayXO[index], style: TextStyle(color: Colors.white, fontSize: 40),),
                    ),
                  ),
                );
              }
            ),
          ),
          Expanded(
            child: Container(
            )
          ),
        ],
      ),
    );
  }

  void _tapped (int index) {
    setState(() {
      if (oTurn && displayXO[index] == '') {
        displayXO[index] = 'O';
        filledboxes += 1;
      }else if (!oTurn && displayXO[index] == '') {
        displayXO[index] = 'X';
        filledboxes += 1;
      }

      oTurn = !oTurn;
      _checkWinner();
    });
  }

  void _checkWinner(){

    if (displayXO[0] == displayXO[1] &&
        displayXO[0] == displayXO[2] &&
        displayXO[0] != '') {
      _showinDialog(displayXO[0]);
    }
    if (displayXO[3] == displayXO[4] &&
        displayXO[3] == displayXO[5] &&
        displayXO[3] != '') {
      _showinDialog(displayXO[3]);
    }
    if (displayXO[6] == displayXO[7] &&
        displayXO[6] == displayXO[8] &&
        displayXO[6] != '') {
      _showinDialog(displayXO[6]);
    }
    if (displayXO[0] == displayXO[3] &&
        displayXO[0] == displayXO[6] &&
        displayXO[0] != '') {
      _showinDialog(displayXO[0]);
    }
    if (displayXO[1] == displayXO[4] &&
        displayXO[1] == displayXO[7] &&
        displayXO[1] != '') {
      _showinDialog(displayXO[1]);
    }
    if (displayXO[2] == displayXO[5] &&
        displayXO[2] == displayXO[8] &&
        displayXO[2] != '') {
      _showinDialog(displayXO[2]);
    }
    if (displayXO[6] == displayXO[4] &&
        displayXO[6] == displayXO[2] &&
        displayXO[6] != '') {
      _showinDialog(displayXO[6]);
    }
    if (displayXO[0] == displayXO[4] &&
        displayXO[0] == displayXO[8] &&
        displayXO[0] != '') {
      _showinDialog(displayXO[0]);
    }
    else if(filledboxes == 9){
      _shodrawDialog();
    }
  }

  void _shodrawDialog(){
    showDialog(
      barrierDismissible: false,
      context: context,
       builder: (BuildContext){
        return AlertDialog(
          title: Text('DRAW'),
          actions: <Widget>[
            TextButton(
              onPressed: (){
                _clearBoard();
                Navigator.of(context).pop();
              }, 
              child: Text('Play Again'))
          ],
        );
       }
    );
  }

  void _showinDialog(String winner){
    showDialog(
      barrierDismissible: false,
      context: context,
       builder: (BuildContext){
        return AlertDialog(
          title: Text('WINNER IS: ' + winner),
          actions: <Widget>[
            TextButton(
              onPressed: (){
                _clearBoard();
                Navigator.of(context).pop();
              }, 
              child: Text('Play Again'))
          ],
        );
       }
    );

    if(winner == 'O'){
      oScore += 1;
    }else if(winner == 'X'){
      xScore += 1;
    }
  }

  void _clearBoard(){

    setState(() {
      for(int i=0; i<9; i++){
      displayXO[i] = '';
    }
    });

    filledboxes = 0;
  }
}